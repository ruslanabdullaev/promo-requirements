# Требования к верстке промо-сайтов

### Содержание
  
   - [Назначение документа](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_1)
   - [Требования к поставке кода](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_2)
      - [Плохая поставка кода](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_3)
      - [Хорошая поставка кода](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_4)
   - [Референсы](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_5)
   - [Общие положения](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_6)
   - [Стандартизированные решения](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_7)
      - [Псевдо-многостраничность](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header--_1)
      - [Номера телефонов](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_8)
      - [Ссылки на email-адреса](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-email-)
      - [Формы обратной связи](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_9)
      - [Сообщения об ошибках](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_10)
      - [Сообщения об успешной отправке](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_11)
      - [Popup-окна](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-popup-)
   - [Общая структура файлов](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_12)
   - [HTML](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-html)
      - [Обязательные составляющие HTML-документа](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-html-)
      - [Мета-теги](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header--_2)
      - [Формы](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_13)
      - [Комментарии в HTML/XML](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-htmlxml)
      - [Микроформаты](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_14)
      - [Требования к поставке HTML-кода](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-html-_1)
   - [CSS](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-css)
      - [Формат записи CSS-правил](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-css-)
      - [Обязательный набор тегов для контента](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_15)
      - [Кавычки](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_16)
      - [Комментарии в CSS](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-css_1)
      - [Применение хаков](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_17)
      - [Препроцессоры CSS](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-css_2)
      - [Требования к поставке CSS-кода](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-css-_1)
   - [JavaScript](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-javascript)
      - [Рекомендуемые библиотеки](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_18)
      - [Требования к коду общего назначения](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_19)
      - [Требования к поставке JavaScript-кода](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-javascript-)
   - [Изображения](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_20)
   - [Чек-лист для подготовки Photoshop-макета к передаче на вёрстку](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header--photoshop-)

### Назначение документа 

Данный документ носит рекомендательный характер за исключением тех случаев, когда явно указано, что та или иная его 
часть является обязательным требованием.

Необходимость создания этого документа обусловлена появлением внешних подрядчиков, создающих верстку для промо-страниц 
проекта [Promo](http://promo.softline.ru)

Поскольку все промо-страницы находятся в одной экосистеме и не являются изолированными сайтами, выдвигается ряд 
требований, соответствие которым улучшит общую структуру проекта, обеспечит легкость его поддержки и своевременность 
реакции на изменения.

Подрядчикам, наиболее полно соблюдающим указанные рекомендации, должно отдаваться предпочтение при появлении новых задач 
на верстку промо-страниц.

### Требования к поставке кода

#### Плохая поставка кода

Zip-архив, содержащий в себе весь код, является __наименее__ приемлемой формой поставки кода проекта, поскольку в случае
возникновения доработок необходимо ожидать передачу новой версии этого архива подрядчиком, и невозможно выяснить,
в каких именно файлах какие именно изменения произошли, чтобы переинтегрировать только требующие правок участки кода
вместо полной реинтеграции. Данного способа следует максимально избегать. 

#### Хорошая поставка кода 

Репозиторий на [bitbucket](https://bitbucket.org) является __наилучшим__ вариантом поставки кода. Подрядчику следует 
сделать репозиторий с кодом публичным, доступным на чтение и предоставить менеджеру ссылку на репозиторий. 
Менеджер передаст эту ссылку ответственным разработчикам.

### Референсы

Перед началом выполнения работ по верстке промо-страниц рекомендуется ознакомиться с некоторыми уже реализованными 
проектами, в которых применяются технические решения, являющиеся стандартом для платформы Promo.

* [Vmware](http://promo.softline.ru/vmware) 
* [Visual Studio 2017](http://vstudio.softline.ru/)
* [Kerio](http://promo.softline.ru/kerio)
* [CorelDRAW Graphics Suite 2017](http://promo.softline.ru/coreldraw)
* [Cisco WebEx](http://webex.softline.ru)

### Общие положения
Все текстовые файлы ДОЛЖНЫ иметь кодировку UTF-8 without BOM.

Для отбивки ДОЛЖНЫ использоваться табы.

Все текстовые файлы ДОЛЖНЫ использовать Unix LF формат для завершения строк

РЕКОМЕНДУЕТСЯ максимально разделять HTML-разметку, CSS-стили и JavaScript код. Для разных технологий СЛЕДУЕТ использоваться отдельные файлы.

Имена тегов во всех технологиях ДОЛЖНЫ писаться в нижнем регистре.


### Стандартизированные решения

> Одними из главных требований к промо-страницам является блочная верстка и адаптивность под мобильные устройства. 
 
> Если по каким-то причинам заказчики не предоставили подрядчикам дизайн-макеты под мобильные устройства, 
> следует настойчиво попросить их об этом. Только в случае, если заказчик явно указывает, 
> что мобильная верстка на промо-странице не нужна, это требование можно игнорировать.

Весомым преимуществом при создании верстки является следование методологии [БЭМ](https://tech.yandex.ru/bem/).  

#### Псевдо-многостраничность

На примере промика [Vmware](http://promo.softline.ru/vmware) можно увидеть, как реализуется псевдо-многостраничность 
при переключении по вкладкам продуктов в верхней части экрана

![Псевдо-многостраничность](https://bitbucket.org/ruslanabdullaev/promo-requirements/raw/master/images/pseudo-multipage.jpg)

#### Номера телефонов

Номера телефонов должны являться кликабельными ссылками, по нажатии на которые пользователь получит номер телефона
на своем мобильном устройстве и сможет совершить звонок

```html
<a class="b-link" href="tel:+74952320023">+7 (495) 232-0023</a>
``` 
#### Ссылки на email-адреса

Ссылки на email-адреса должны быть полноценными сылками. Простой текст или тег ```<span>``` недопустимы

```html
<a class="b-list__item-text b-link" href="mailto:email@server">
    email@server.com
</a>
```
#### Формы обратной связи

Через формы обратной связи осуществляется отправка данных на бакенд, где они обрабатываются и сохраняются в базу данных,
а заинтересованные пользователи получают соответствующие уведомления.

>НЕ требуется писать никакой код, осуществляющий отправку данных на бекенд, тем более, что для AJAX-запросов 
>используется собственный фреймворк, и традиционные JQuery-ajax запросы не будут работать. 
>
>Вся интеграция с бакендом осуществляется backend-разработчиками. От фронтенд-разработчика требуется только реализация 
внешнего вида формы и элементов управления ею.

#### Сообщения об ошибках

![Формы обратной связи](https://bitbucket.org/ruslanabdullaev/promo-requirements/raw/master/images/popup.jpg)

Обратите внимание на тот факт, что в верстке обязательно должны присутствовать стили для отображения и примеры 
оформления сообщений об ошибочно заполненных полях формы. Стандартным правилом является отображение сообщения о
неверном заполнении поля непосредственно под ним. [См. примеры реализации](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_5)

#### Сообщения об успешной отправке

![Сообщение об успешной отправке](https://bitbucket.org/ruslanabdullaev/promo-requirements/raw/master/images/form-success.jpg)

Каждую форму, вне зависимости от того, расположена она в Popup-окне, или непостредственно на странице, должно сопровождать
сообщение об успешной отправке. [См. примеры реализации](https://bitbucket.org/ruslanabdullaev/promo-requirements/#markdown-header-_5) 

#### Popup-окна

Как правило, в Popup-окнах размещаются формы заказов/запросов/обратной связи вместе с сообщениями
об успешной отправке. Мы рекомендуем использовать библиотеку [jquery.arcticmodal](https://arcticlab.ru/arcticmodal/)
для работы с popup-окнами.

### Общая структура файлов

Необходимо выстраивать структуру проекта в соответствии со следующими требованиями. Соблюдайте названия
и верхний уровень иерархии директорий. Ограничений на структуру вложенных папок нет:
   
   - __index.html__ -  верстка
   - __css__
      - style1.css
      - style2.css
   - __images__
      - image1.png
         - __subdirectory__
            - image2.png
   - __javascript__
      - library1.min.js
      - library2.min.js
      - common.js
   - __fonts__ - шрифты
   - __files__ - дополнительные файлы контента, например, *.pdf

### HTML

При написании HTML-кода ОБЯЗАТЕЛЬНО следовать версии стандарта HTML5.

Разметка ДОЛЖНА быть семантичной.

HTML-код ДОЛЖЕН быть валидным.

РЕКОМЕНДУЕТСЯ использовать XHTML-валидный синтаксис. Закрывать теги в т.ч. пустые, использовать кавычки и писать значения для всех атрибутов и т.п.

Всегда СЛЕДУЕТ использовать двойные кавычки для атрибутов.

#### Обязательные составляющие HTML-документа

Доктайп:
```html
<!DOCTYPE html>
```

Объявление HTML:
```html
<html lang="ru-RU" id="nojs">
```

Разделы head и body:
```html
<head>
	...
</head>
<body>
	...
</body>
```

Заполненный тег заголовка страницы title:
```html
<title>Title</title>
```

#### Мета-теги

Кодировка:
```html
<meta charset="utf-8" />
```

Специфичные для Internet Explorer:
```html
<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<!-- указание для IE6-10 переключиться в последний из возможных стандартный режим отображения -->
```
```html
<meta http-equiv="imagetoolbar" content="no" /> 
<!-- отключение всплывающей посказки на изображениях -->
```

Для поисковиков:
```html
<meta http-equiv="keywords" content="" />
<meta http-equiv="description" content="" />
<meta name="description" content="" />
```

Против Skype-плагина:
```html
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
```

Вьюпорт:
```html
<meta name="viewport" content="width=1024" /> 
<!-- для фиксированных макетов -->

<meta name="viewport" content="width=device-width, initial-scale=1" />  
<!-- для адаптивной верстки -->
```

Также, в head-области ОБЯЗАТЕЛЬНО присутствие строки, проверяющей JavaScript, и при его наличии, переключающая идентификатор в теге html на «js» (изящная деградация JavaScript):
```html
<script type="text/javascript">document.documentElement.id = "js";</script>
```

Подключение минифицированного и оптимизированного файла стилей:
```html
<link href="css/template_styles.css" rel="stylesheet" />
```

Файлы стилей для старых версий IE (также минифицированные) подключаются через условные комментарии. Пример:
```html
<!--[if lt IE 8]>
	<link href="css/template_styles_ie.css" rel="stylesheet" />
<![endif]-->
```

Javascript, в т. ч. jquery СЛЕДУЕТ подключать в конце body:
```html
<script src="js/jquery/1.10.2/jquery.min.js"></script>
<script src="js/init.js"></script> <!-- Инициализация плагинов -->
<script src="js/someJavascript.js"></script> <!-- Production-ready  -->
<script src="js/demo.js"></script>  <!-- исключительно для демонстрации работоспособности верстки в динамичном окружении-->
```

Таким образом, HTML-документ с минимальным набором обязательных элементов и атрибутов выглядит примерно так:
```html
<html lang="ru-RU" id="nojs">
<head>
	<meta charset="utf-8" />
	<title>Title</title>
	<meta http-equiv="keywords" content="" />
	<meta http-equiv="description" content="" />
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="imagetoolbar" content="no" />
	<script type="text/javascript">document.documentElement.id = "js";</script>
	<link href="css/template_styles.css" rel="stylesheet" />
	<!--[if lt IE 8]>
		<link href="css/template_styles_ie.css" rel="stylesheet" />
	<![endif]-->
</head>
<body>
	...
	<script src="js/jquery/1.10.2/jquery.min.js"></script>
</body>
</html>
```
#### Формы

Каждый элемент формы ДОЛЖЕН иметь id и, если это необходимо, работающую метку (label).

Схема именования id полей:
```html
fFormName-FieldName
```

Примеры:
```html
fRegistration-Surname
fFastOrder-CustomerPosition
```

#### Комментарии в HTML/XML

Формат комментариев:
```html
<!-- .b-header -->
<div class="b-header">
	...
</div>
<!-- /.b-header -->

```

Комментарии для структурирования кода писать НЕ РЕКОМЕНДУЕТСЯ.

В комментариях МОЖНО оставлять для разработчиков подсказки о значении классов или о чем-то еще, но лучше донести эту информацию другим образом, потому что очень часто разработчики не открывают исходный html-код.

#### Микроформаты

Для некоторых видов контента СЛЕДУЕТ использовать специальную микро-разметку.

ОБЯЗАТЕЛЬНО при случае использовать следующие микроформаты:

* [Breadcrumbs RDFa](https://developers.google.com/search/docs/guides/enhance-site?visit_id=1-636405823907352970-3154702009&hl=en&rd=1#enable-breadcrumbs) – для навигации «хлебные крошки».
* [hCard](http://microformats.org/wiki/hcard) – для информации о людях и об организациях.
* [hCalendar 1.0](http://microformats.org/wiki/hcalendar) – для информации о событиях.

#### Требования к поставке HTML-кода

Каждая страница промо-сайта должна быть отдельным HTML-файлом. Даже если промо-сайт многостраничный и все его страницы
состоят из многократно переиспользуемых компонентов, каждая страница должна поставляться отдельным полностью собранным 
HTML-файлом.  

### CSS

Валидность CSS приветствуется, но наличие CSS-хаков допускается.

Классы ДОЛЖНЫ именоваться в соответствии с методологией БЭМ 

НЕ ДОЛЖНЫ использоваться селекторы по id. Временное исключение – #nojs.

НЕ ДОЛЖНЫ использоваться селекторы по элементам. Исключение – стилизация элементов внутри контентной области.

НЕ СЛЕДУЕТ использовать !important для усиления специфичности.

Конечные CSS-файлы ДОЛЖНЫ отдаваться в собранном и минимизированном виде.

В качестве минимизатора СЛЕДУЕТ использовать [CSSO](https://github.com/css/csso/)

#### Формат записи CSS-правил

Фигурная скобка всегда пишется на одной строке с именем класса через пробел.

Свойства ДОЛЖНЫ писаться с новой строки с отступом в одну табуляцию.

Пробел между свойством и значением свойства не ставится

Закрывающая скобка ДОЛЖНА ставиться на новой строке.

После каждого свойства (и последнего тоже) ДОЛЖНА ставится точка с запятой.
```css
.className {
	property1:value;
	property2:value;
}
```

При перечислении классов, классы ДОЛЖНЫ писаться с новой строки через запятую.
```css
.className1,
.className2 {
	property:value;
}
```

СЛЕДУЕТ придерживаться канонического порядка записи(сортировки) CSS-правил:
```css
content
box-sizing

display
position

width
height
margin
padding
border
 
font
font-size
line-height

color
font-variant
text-transform
text-decoration
```

Для простых блоков РЕКОМЕНДУЕТСЯ использовать сокращенные записи CSS-правил.
```css
.b-paragraph {
	font:12px/16px Tahoma, Geneva, 'Ubuntu Light', Ubuntu, sans-serif;
	margin:0;
	padding:0;
	border:0;
}
```

При переопределении уже существующих CSS-правил, наоборот, СЛЕДУЕТ вносить точечные изменения.
```css
.b-link {
	border-bottom:1px solid blue;
	&:hover {
		border-bottom-color:red;
	}
}
```

Чаще всего подобную осторожность СЛЕДУЕТ соблюдать в модификаторах узкого назначения.

При перечислении альтернативных гарнитур для шрифта СЛЕДУЕТ указывать схожие системные шрифты для всех основных ОС и ОБЯЗАТЕЛЬНО общее семейство.

#### Обязательный набор тегов для контента

Существует набор элементов контента, для которых ОБЯЗАТЕЛЬНО определять стиль. Необходимо запрашивать дизайн этих элементов у дизайнера, а если это невозможно, определять их на свой вкус.

К обязательным тегам относятся: 
```html
<p>, <h1>-<h4>, <a>, <img>, <ul>, <ol>, <table>
```

#### Кавычки

Всегда СЛЕДУЕТ использовать одинарные кавычки

#### Комментарии в CSS

Формат комментариев
```css
/* .b-header > */
.b-header {
	padding:0;
}
/* .b-header < */
```

НЕ РЕКОМЕНДУЕТСЯ использовать комментарии для структурирования блоков кода.

Комментарии СЛЕДУЕТ использовать исключительно для объяснения сложных или неявных решений.

#### Применение хаков

Хаки для разных версий IE ДОЛЖНЫ быть вынесены в отдельные от основного CSS-файлы.

Хаки для других браузеров СЛЕДУЕТ писать в конце основного файла стилей.

#### Препроцессоры CSS

В случае использования препроцессоров все правила для CSS сохраняют свою силу.

Препроцессоры, которые РЕКОМЕНДУЮТСЯ к использованию:

* [Less](http://lesscss.org/)
* [Stylus](http://stylus-lang.com/)

В случае использования Stylus ОБЯЗАТЕЛЬНО использовать синтаксис со скобками.

#### Требования к поставке CSS-кода

Поставляемый файл стилей должен иметь расширение ```.css``` он может быть минифицированным.

### JavaScript

НЕ СЛЕДУЕТ без необходимости использовать js-полифилы.

Для стандартных задач СЛЕДУЕТ использовать плагины из списка ниже.

НЕ СЛЕДУЕТ использовать Javascript для эмуляции CSS-приемов (например, колонки раскладки одинаковой высоты).

#### Рекомендуемые библиотеки

* [jquery](https://jquery.com/)
* [jquery.arcticmodal](https://arcticlab.ru/arcticmodal/) - для модальных окон    
* [jquery.maskedinput](https://github.com/digitalBush/jquery.maskedinput) - для масок ввода
* [jquery.placeholder](https://github.com/mathiasbynens/jquery-placeholder) - для плейсхолдеров в полях ввода 
* [jquery.validate](https://jqueryvalidation.org/) - для валидации форм
* [bootstrap-datepicker](http://www.eyecon.ro/bootstrap-datepicker/) или форк [bootstrap-datepicker](https://github.com/uxsolutions/bootstrap-datepicker) - датапикер    
* [html5shiv](https://github.com/aFarkas/html5shiv) - совместимость с устаревшими браузерами
* [OwlCarousel](https://owlcarousel2.github.io/OwlCarousel2/) - для создания каруселей
* [select2](https://select2.github.io/) -  для декорирования селектов
* [baron](https://github.com/Diokuz/baron) - кастомизация скроллбара
* [fotorama](http://fotorama.io/) - слайдер для фотогалерии

#### Требования к коду общего назначения

Никаких ограничений для кода общего назначения, который обеспечивает функционирование промо-страницы, нет.

#### Требования к поставке JavaScript-кода

Любой Javascript-код, в том числе общего назначения, должен поставляться как есть, без минификации и без упаковки 
всех ресурсов в один javascript-файл. Каждая библиотека должна быть отдельным файлом. 

### Изображения

Декоративные изображения СЛЕДУЕТ объединять в спрайты.

Прежде всего СЛЕДУЕТ использовать спрайты для иконок.

Основной спрайт-файл с иконками СЛЕДУЕТ называть icons.png.

Для декоративных изображений предельно-малых размеров вместо спрайтов СЛЕДУЕТ использовать base64-кодирование.

### Чек-лист для подготовки Photoshop-макета к передаче на вёрстку

[Основная статья](https://habrahabr.ru/post/353430/)

1. Если дизайнер использовал сетку, все блоки на макете расположены строго по ней.
1. У всех объектов на макете целочисленные размеры.
1. Повторяющиеся элементы на страницах всегда ОДИНАКОВЫЕ.
1. Все слои сгруппированы по папкам и распределены по логике макета. Лишние удалены, похожие — объединены.
1. Отступы от элементов унифицированы.
1. Цвета на макете совпадают с основными цветами проекта.
1. Текст как текст (не растрирован).
1. Эффекты наложения, тени и градиенты не растрированы.
1. Использование эффектов наложения целесообразно.
1. У шрифтов недробные размеры.
1. Шрифты, используемые в проекте, собраны в отдельной папке.
1. Нестандартные шрифты и их начертания проверены на наличие веб-версии. Вес одного нестандартного шрифта не превышает 1 Мб.
1. Межстрочные интервалы и отступы в тексте унифицированы.
1. Все иконки в формате SVG и собраны в одном месте. Наименования иконок одинаковые и понятные, совпадают с наименованием идентичных слоёв на макете.
1. Для всех активных элементов есть слои с ховерами.
1. Объекты, участвующие в в анимациях/интерактивных взаимодействиях, разбиты послойно. Для баннеров — аналогично.
1. К анимациям и интерактивным взаимодействиям прописаны комментарии и указаны примеры, как это должно выглядеть.
1. Для макета создан гайдлайн с палитрой цветов проекта и стилями текста.